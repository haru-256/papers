# Inception Score について

Inception Score はGANで，Generatorのモデル評価に使われる指標である．



## 概要
提案論文：[[Tim Salimans, 2016/06] Improved Techniques for Training GANs](https://arxiv.org/abs/1606.03498)

ISでは以下の２つの基準を用いてGANの性能を評価します．
- 生成画像の質
- 多様性

まず，生成画像 $\boldsymbol{x}_i$ に対してInception v3 を適用し，条件付き分布  $ p(y| \boldsymbol{x}_i) $ をえます．生成画像の質が良く，意味のある物体を含むならば $p(y| \boldsymbol{x}_i) $  は低いエントロピーを持つはずです．さらに，良い生成モデルでは多様性があることを期待し，$p(y) = \sum_{\boldsymbol{x}_i} p(y|\boldsymbol{x}_i)p(\boldsymbol{x}_i)  $ は高いエントロピーを持っていることになります．

これらのことを踏まえて，Inception Score は以下のように定義されます．ISが高ければ高いほど良い生成モデルとなります．

$$
\rm{IS}(G) = \exp \left\{\mathbb{E}_{\boldsymbol{x}_i \sim G} \left[KL \left(p(y | \boldsymbol{x}_i) \| p(y)\right)\right] \right\}
$$
また，KL は以下のようにして計算されます．ここで，$C = \{ 1, 2, \dots, 1000 \}$ はImageNet のクラス数を表します．

$$
KL \left(p(y | \boldsymbol{x}_i) \| p(y) \right) = \sum_{y \in C} p(y | \boldsymbol{x}_i) \ln \frac{p(y|\boldsymbol{x}_i)}{p(y)}
$$


提案論文では，IS はAMTの人手による評価と相関があるということ分かったと主張しています．

また論文では，生成モデルの多様性を測るためにも十分なデータ数（5万枚程度）でISを評価する必要があると主張しています．

実際にISを計算する際には（$\boldsymbol{x}$ に関する経験分布） $p(\boldsymbol{x}_i) = 1/N$ を利用し，周辺分布 $p(y) = \sum_{\boldsymbol{x}_i} p(y|\boldsymbol{x}_i)p(\boldsymbol{x}_i)  $  を以下のようにして近似します．

$$
\hat p(y) = \frac{1}{N} \sum_{\boldsymbol{x}_i} p(y|\boldsymbol{x}_i)
$$

## 問題点

- どの問題設定においても現れる問題点

  IS は本物画像に依存しないため，生成したい物体と全く違う物体を上手く捉えた生成モデルでも，ISはよくなる．

- pose2human の問題設定での問題点

  Inception v3 は ImageNet で学習されたものですが，ImageNetは動物・装置・乗り物などが多く人物がいないため，pose2humanの生成画像の質を Inception v3で測るのは難しい？後述のFIDでもInception v3 を用います．vid2vidでは量的評価にFIDを用いており，その時はInception v3 ではなく， video recognition CNNs を用いました．