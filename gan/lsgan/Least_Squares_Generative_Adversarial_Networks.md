# Least Squares Generative Adversarial Networks

# Info

- Date : 11/ 2016
- Authors : [Xudong Mao](https://arxiv.org/search/cs?searchtype=author&query=Mao%2C+X), [Qing Li](https://arxiv.org/search/cs?searchtype=author&query=Li%2C+Q), [Haoran Xie](https://arxiv.org/search/cs?searchtype=author&query=Xie%2C+H), [Raymond Y.K. Lau](https://arxiv.org/search/cs?searchtype=author&query=Lau%2C+R+Y), [Zhen Wang](https://arxiv.org/search/cs?searchtype=author&query=Wang%2C+Z), [Stephen Paul Smolley](https://arxiv.org/search/cs?searchtype=author&query=Smolley%2C+S+P)
- Journal reference: CVPR 2017

# どんなもの？



# 先行研究と比べてどこがすごい？



# 技術の手法のキモはどこ？



# どうやって有効だと検証した？



# 議論はある？



# 次に読むべき論文は？



