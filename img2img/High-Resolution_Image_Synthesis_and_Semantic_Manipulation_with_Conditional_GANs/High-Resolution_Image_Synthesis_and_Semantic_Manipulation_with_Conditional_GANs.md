# High-Resolution Image Synthesis and Semantic Manipulation with Conditional GANs

# Info

- Data : 11/30/2017
- Authors : Ting-Chun Wang, Ming-Yu Liu, et al.
- Journal reference : CVPR 2018

# どんなもの？

新たなadversalial loss とmulti-scale generator, discriminator architectures を用いて semantic label map から高解像度（2048x1024）の画像を生成する手法を提案した．

さらに提案フレームワークを拡張させ，２つの機能を用いてinteractive な視覚的操作を可能にした．

1. object segmentation instance 情報を統合し，オブジェクト操作を可能にさせた．
2. ある１つの入力に対して多様な出力を生成させる手法を提案し，ユーザーがインタラクティブに物体の外観を変更することを可能にした．

# 先行研究と比べてどこがすごい？

主に比較対象とする先行研究は先行研究1: [Q. Chen](https://arxiv.org/abs/1707.09405) のperceptual loss を改良した手法．

その先行研究で，adversarial training はunstable であり，高解像度の画像生成タスクにおいて学習が失敗しやすいということが分かった．代わりに彼らはsynthesize images について改良した perceoutual loss を用いた．これは高解像度（2048x1024）の画像を生成することができるが，しばしば物体の詳細やrealistic なtextures は欠落する．

本論文では上のSoTA の手法の主な問題点である以下の２つについて取り組んだ．

1. GANs を用いた高解像度の画像生成
2. 先行研究での生成された高解像度画像での，物体の詳細とrealistic texture の欠落

新たなロス関数とcoarse-to-fine なGenerator，multiscale Diiscriminatorを用いることで，先行研究と比べよりrealistic な高解像度（2048x1024）の画像生成を実現した．さらに，生成画像内の物体レベルでの操作を可能にし，ユーザーが最小の努力で素早く新たなシーンの画像を作ることを可能にした．

# 技術や手法のキモはどこ？

pix2pix を直接用いて高解像度の画像を生成させようとしたが，既存のpix2pix学習の不安定さと満足いかない質の画像を生成することが分かった．

そこで本論文ではpix2pix をcoarse-to-fine generator とmulti-scale discriminator，robust adversarial learning objective function を用いてpix2pix を改良した．

## pix2pix について

pix2pix はgeneratorにU-Net 構造を，disciriminator にPathGAN を採用した．しかし，pix2pix の実験では最大でも256x256 の解像度であった．そこで，試しにpix2pix のモデルのままで高解像度の画像生成を実験してみたところ，学習の不安定さと生成画像の質の悪さが分かった．

## Improving pix2pix for Photrealism and Resolution

本論文ではpix2pix をcoarse-to-fine generator と multi-scale discriminator ，そして robust adversarial lerning objective function を用いて改良した．

### Coarse-to-fine generator

まず，generator $G$ を２つのサブネットワーク: $G_1$ (global generator network), $G_2$ (local enhancer network)に分ける．generator は Fig. 3 のように１組みで与えられる $G = \{ G_1, G_2\}$．

<img src="figures/fig3.png"></img>

$G_1$ $G_2$ の入力画像が2048x1024 のときは1/2 にdownsampling した1024x512 の画像を入力し1024x512 の画像を出力する．そして，$G_2$ の出力画像は$G_1$ の出力画像の縦横を2倍にした2048x1024 の画像を出力する．より高解像度の画像を作る際は 追加の local enhancer network を用いる．具体例は以下参照．
>   For example, the output image resolution of the generator $G = \{G_1, G_2\}$ is 2048 × 1024, and the output image resolution of $G = \{G_1, G_2, G_3\}$ is 4096 × 2048.



$G_1 G_2,$ は先行研究2 （この論文では最大で512x512 の解像度でneural style transfer が可能であることを示した）を基に作られている．詳細は以下参照.

>   ​    Our global generator is built on the architecture proposed by Johnson et al. [22], which has been proven successful for neural style transfer on images up to 512 × 512. It consists of 3 components: a convolutional front-end $G^{ (F )}_1$, a set of residual blocks $G^{(R)}_1$ [18], and a transposed convolutional back-end $G^{(B)}_1$. A semantic label map of resolution1024×512 is passed through the 3 components sequentially to output an image of resolution 1024 × 512.
>   ​    The local enhancer network also consists of 3 components: a convolutional front-end $G^{(F)}_2$, a set of residual blocks $G^{(R)}_2$, and a transposed convolutional back-end $G^{(B)}_2$. The resolution of the input label map to $G2$ is 2048 × 1024. Different from the global generator network, the input to the residual block $G^{(R)}_2$ is the element-wise sum of two feature maps: the output feature map of $G^{(F )}_2$, and the last feature map of the back-end of the global generator network $G^{(B)}_1$. This helps integrating the global information from $G1$ to $G2$.

#### 学習方法

まずはじめに1024x512で $G_1$ を学習し，その後$G_1$ に  $G_2$ をくっつけ，$G_1$ の重みを固定した上で2048x1024で $G_2$ のみを学習させる ．最後に，全体 $G_1, G_2$ でfine-tune する？
参考：https://www.slideshare.net/ssuser86aec4/cvpr2018-pix2pixhd-cv-103835371

### Multiscale Discriminator

高解像度な本物画像と生成画像を区別するためには，より大きなreceptive field を持つDiscriminatorが必要となる．これは，deeper network または larger convolutional kernelsのどちらかを要す．しかし，２つともnetwork のcapacity を高め，過学習を引き起こす可能性がある．またより大きなメモリーが必要となるが，高解像度の画像生成を行うGenerator のためにすでに使われてしまっている．

このため本論文では３つの全く同じ構造を持つDiscriminator $D_1, D_2, D_3$を扱う．３つとも構造は同じだが，扱う画像サイズが異なる．具体的には画像を 2 downsampling したものと 4 downsampling したものを作り，3 scale（2048x1024 が生成したい画像だとすると，2048x1024, 1024x512, 512x256の３つ）のimage pyramid を作る．そして，$D_1, D_2, D_3$ はそれぞれ，異なるscale の画像で本物と偽物を区別するように学習する．

Multiscale Discriminator 無しでは多くの repeated pattern が生成画像に現れることが分かった．

#### Loss

Multiscale Discriminator の場合，Loss 関数は以下のようになる．

$$
\begin{align}
\min_{G} &\max_{D_1, D_2, D_3} \sum_{k=1}^{3} \mathcal{L}_{GAN} (G, D_k) \\
\text{ここで, }& \notag\\
\mathcal{L}_{GAN}(G, D_k) &= \mathbb{E}_{(s, x)} [\log D_k(s, x)] + \mathbb{E}_{s} [\log(1-D_k(s, G(s))]
\end{align}
$$
ここでデータセットは$\{(s_i, x_i)\}$ で与えられているとし，$s_i$ はsemantic label であり，$x_i$ は $s_i$ に対応する画像とした．<span style="color:red">しかし，本論文で使用するGANロスはLSGANの形式をとっており，この式とは異なる．</span>

## Improving adversarial loss

本論文では式（1）に feature matching loss based on discriminator を付け加えた．具体的には，Discriminator の複数の層から特徴量を抽出し，real 画像と生成画像の中間表現をマッチするように学習させた．

Discriminator $D_k$ の $i$ 番目の層のfeature extractor を $D_k^{(i)}$ と表すと，feature matching loss $\mathcal{L}_{FM}$ は以下のように定義される．
$$
\mathcal{L}_{FM} =  \mathbb{E}_{(s_i, x_i)}\sum_{k=1}^K \sum_{i=1}^T \frac{1}{N_i} \left[ \| D_k^{(i)}(s_i, x_i) - D_k^{(i)}(s_i, G(s_i)) \|_1 \right]
$$

ここで，$T$ はDiscriminator の層数であり，$N_i$  は各層の出力の要素数を表す．また $K$ はDiscriminatorの数． $\mathbb{E}_{(s_i, x_i)} $ とあるが，結局は$1/K \times 1/T$ をする． <span style="color:red">注意</span> **しかし，実際の[コード](https://github.com/NVIDIA/pix2pixHD/blob/master/models/pix2pixHD_model.py#L179-L187)では$1/K \times 4/T $  をしている．**

よって最終的な目的関数は以下のようになる．

$$
\min_G \bigg(\Big(\max_{D_1, D_2, D_3} \sum_{k=1}^3 \mathcal{L}_{GAN} (G, D_k) \Big)  + \lambda \mathcal{L}_{FM} (G, D_k)\bigg)
$$

$\lambda$ は２項の重要度を制御する係数である．著者実装の場合，default値は10

## Using Instance Maps

### 課題
Semantic labels だけだと同じカテゴリー内の物体を差別化できない．特にstreet sceneでは路上に止められた車が連なっていたり，歩行者が重なったりするため，境目がうまく生成されない．

### 解決策

この論文ではSegmentic label では扱えなくてInstance mapによって扱えるものはObject Boundaryと仮定している．Object Boundary が重なった同一物体のラベルの境目をモデルに教え，生成される画像の物体の境目が綺麗になる．ただし，Instance Mapをそのままモデルに入力すると実装がしにくい（画像によって，写っている物体の数が異なるため？）．必要な情報はObject Boundary Mapのみ．そこで，Instance MapよりBoundary Mapを作成し，入力するのはBoundary Map（１次元チャネルの画像）のみにする．

<img src="https://imgur.com/wPx0EXT.png" width=500>



### 実装はどうやって？
Instance Mapにおいて，上下左右４つのピクセルで１つでもObject IDが異なっていれば，そのピクセルを１に，そうでなければ０にしてObject Boundary Mapを作成している．

Object Boundary Mapはone-hot-vectorで表現されたSemantic labelと（チャネル方向で）結合し，結合したものをgeneratorに入力している．

同様に，discriminatorへの入力も，Object Boundary Map，Semantic labelとreal/synthesized imagesをチャネルごとに結合したものとする．

### 効果は？

以下の図参照

<img src="https://imgur.com/0cvI18A.png" width=500>

## Learning an Instance-level Feature Embedding

### 課題

Semantic Labelからの画像生成はone-to-many mappuing probremである．そのため理想の画像生成アルゴリズムは，同一のsemantic labelからrealisticで多様な画像を生成する能力がなければならない．最近の先行研究は多様性の問題に取り組んではいるが，本論文のタスクにおいては以下の大きく２つの理由で適していないと言える．

1. モデルが生成する画像の種類について，ユーザーが直感的な操作ができない．
2. 先行研究の方法は全体の色やtextureの多様性に焦点を当てており，object-levelの操作を可能にしない．

### 解決方法

generatarの入力にlow-dimensional feature channelsを追加することで多様性と，直感的な操作を可能にした．low-dimensional feature channelsは連続値のため，理論的にはこの方法は無限種の画像を生成する能力を持つ．

### 実装

low-dimensional feature channelsを得るために，画像内の各インスタンス の ground truth target に対応するlow-dimensional feature を見つけるように  encoder network $E$ を学習する． encoder network $E$ は標準のencoder-decoderネットワークである．

low-dimensional feature が各インスタンスで定数（一定）であるように次のことをした．

- encoder network $E$ の出力の後に，object instance のaverage featureを計算するようにinstance-wise average pooling layerを追加する．

その後，average featureをinstangeの全ピクセルに対してブロードキャストする．

以上の操作を可視化したのが以下の図．

<img src="https://imgur.com/0bYHyCP.png" width="500">

### 学習

式（４）の$G(s)$ を$G(s, E(x))$に変更する．
$$
\min_G \bigg(\Big(\max_{D_1, D_2, D_3} \sum_{k=1}^3 \mathcal{L}_{GAN} (G(\boldsymbol{s}, E(\boldsymbol{x})), D_k) \Big)  + \lambda \sum_{k=1}^3 \mathcal{L}_{FM} \Big(G(\boldsymbol{s}, E(\boldsymbol{x})), D_k\Big)\bigg)
$$

ここでデータセットは$\{(s_i, x_i)\}$ で与えられているとし，$s_i$ はsemantic label であり，$x_i$ は $s_i$ に対応する画像とした．

そして，encoder network $E$ はgeneratorとdiscriminatorとつなげて共に学習させる．

### 推論

学習後，まず学習データの全インスタンスについてに関してencoder network $E$ を適用し，得られた特徴量を記憶しておく（おそらく，Semantic labelのカテゴリごとに全インスタンスのaverage featureを記憶）．そして，各Semantic labelでK-meansを適用しK個のクラスターにクラスタリングする．

そのため，average featureの各clusterはsemantic labelに関する特徴量を符号化したものとなる．

推論時ではそのclusterの１つを選択し，それをaverage featureとして使用する．このaverage featureはlabel mapとBoundary mapと結合し，generatorの入力として使用する．

# どうやって有効だと検証した？

## 検証方法

本論文で評価する方法は以下の３つ

1. 先行研究と定量的に比較する方法
2. 主観的な人間の知見による方法
3. インタラクティブなオブジェクト操作結果

## 実験条件

- GANのロス $\mathcal{L}_{GAN}(G, D_k)$ には安定性のためLSGANを使用．
- 全実験において式（５）の係数 $\lambda$ は $\lambda = 10$ とし，クラスター数$K$ は$K=10$ とした．
- また，encoder network $E$ の出力，low-dimensional featureは3次元ベクトルとした．（つまり，encoder network $E$ の出力は３チャネルの３次元テンソル）
- 式（５）のロスに，以下のperceptual lossを追加．ただし$\lambda=10$，$F^{(i)}$は$N$ 層のうち $i$ 番目のlayerを表す．  また，$M_i$ は $F^{(i)}$ の総ユニット数を表す．論文ではこのロスを加えたものを**ours** ，加えないものを**ours(w/o VGG loss)** とした．
$$
\lambda \sum_{i=1}^{N} \frac{1}{M_i} \bigg\| F^{(i)}(\boldsymbol{x}) - F^{(i)}\Big(G\big(\boldsymbol{s}, E(\boldsymbol{x})\big)\Big) \bigg\|_1
$$
論文では以上のように書いてあるが，実際のコードでは全層の特徴量を用いて計算しておらず，ある特定の層のみ用いている．またその層の位置に応じてさらに重み付けしている．

   - https://github.com/NVIDIA/pix2pixHD/blob/master/models/networks.py#L388-L418
   - https://github.com/NVIDIA/pix2pixHD/blob/master/models/networks.py#L119,%20L126



$$
\min_{G}\  \sum_{k=1}^{3} \mathcal{L}_{GAN} (G, D_k)  + \lambda \sum_{k=1}^3 \mathcal{L}_{FM} \Big(G, D_k\Big) + \lambda \mathcal{L}_{P}(G,x)\\ 

\max_{D_1,D_2,D_3}\  \sum_{k=1}^{3} \mathcal{L}_{GAN} (G, D_k)
$$

$$
 \mathcal{L}_{GAN-D}(D_k) = \frac{1}{2}\mathbb{E}_{(s, x)}
			\left[\left(D_k(s,x) - b\right)^2\right]+\frac{1}{2}\mathbb E_{s}\left[
				\left(D_k(s,G(s)) - a\right)^2
			\right]\\
 \mathcal{L}_{GAN-G} = \frac{1}{2}\mathbb E_{s} \left[ \left(D_k(s,G(s)) - 1\right)^2 \right]\\
$$
## データセット

主に[Cityscape（おそらく gtFine_trainvaltest.zip (241MB) [md5]）](https://www.cityscapes-dataset.com/downloads/) と NYU Indoor RGBD を使用．追加実験としてADE20K,  Helen Face を使用

僕の実験ではCityscape を用いてsemantic label to realistic image を，CelebA-HQを用いて edge2pix を行う（edgeとなる face landmarkは[ここ](https://www.learnopencv.com/facemark-facial-landmark-detection-using-opencv/)参照．C++を学ぶ必要がありそう）．

## ベースライン

基本的に以下の２つを比較対象としている．

1. pix2pix （default settingで高解像度画像を生成するように）
2. CRN（先行研究１のモデル．著者の公式の実装モデルを利用）



## 1. Quantitative Comparisons

pix2pixと同じ評価方法を用いて，定量的に評価した．具体的な比較方法は以下の通り．

> 生成された画像に対して semantic segmentation（本実験ではPSPNet）を適用し，その結果がground truth(訓練での入力)とどれくらいあっているかを次の評価基準で比較する．
>
> 1. Pixel accuracy : ピクセルごとの正解不正解を平均化したもの
> 2. mean IoU([intersection-over-union](https://www.pyimagesearch.com/2016/11/07/intersection-over-union-iou-for-object-detection/) : IoUをオブジェクトクラス数で平均化したもの

**結果**

<img src="https://imgur.com/M83Sp3m.png" width=500>

この結果はオリジナル画像（本物）と比較して，結果がかなり近かった．つまり，この方法での評価的には本手法で生成した画像は realism の上限に達した．

## 2. Human Perceptual Study

人間の主観的評価のため，Cityscapes datasetsでMTurkを用いてA/Bテストを行った．具体的には以下の２種類の評価方法を実行した（先行研究１と同じ方法）．

1. **Unlimited time** : 被験者？は一度に二枚の画像（それぞれ同じsegmentation mapから，異なる手法で生成されたもの）を与えられ，無制限内でどちらの画像がより本物かを選ぶ実験．左右，画像の順番などはランダム化されている．全500枚のtest images は各10回ずつ比較され，計5000人の人間に評価された．この実験ではinstance mapを使用していない．**結果がTable 2.**
   <img src="https://imgur.com/RPLqWek.png" width=500>
2. **Limited time** : 本実験での生成画像とCRNの結果画像，本物と制限時間付きで比較する．各実験において２種の方法の結果を示し，どのくらい早くに２つの画像の差異に気づくのかを評価する．制限時間は 1/8~8s 間でランダムに選択される．**結果がFig 7.**
   <img src="https://imgur.com/fcIq1bE.png" width=500>

## その他で気づいたこと

### Analysis of the loss function

unlimited time 実験でロス関数の各項の重要度を調べた．ロス関数は結局以下の３つであった．

1. GAN ロス（LSGAN）
2. Discriminator-based feature matching loss（FMロス）
3. VGG perceptual loss

**結果**

FMロスを加えることで，GAN loss のみよりかはかなり改善された．また，perceptual lossを加えることでさらに改善された．しかし，perceptual lossはあまり重要とは考えられなく，GAN loss + FMだけでも十分視覚的に魅力的な画像が作れる．

<img src="https://imgur.com/EwXvBlc.png">

### Using instance map

Instance map を使用した場合とそうでない場合の２種の画像を作成し，車の領域に焦点を当て，評価の参加者に聞いたところ，64.34%でInstance map を使用した場合が良いと答えた．これは特に物体の境界あたりにおいてrealism が改善されたことを示している．

### Analysis of the generator

異なるgenerator（U-Net, CRN）で，他の全ての構成を固定した上でsegmentation-score, human perceptual study を比較した．結果は以下の通り．

<img src="https://imgur.com/S4ltN77.png" width=500>

### Analysis of the discriminator

他の条件を同じにして，single Discriminator とmulti-scale Discriminators の２種類をsegmentation-scoreで比較したところ，学習の安定性に加えて，より高解像度な画像が生成できた．また，A/B テストを行った結果，69.2% の人がmulti-scale Discriminatorの画像のが良いと答えた．

<img src="https://imgur.com/LQI7YfT.png" width=500>

# 実装

付録よりわかった本研究の詳細な実験条件を示す．

## 最適化手法

Adam ．learning rate = 0.0002. はじめの100epoch は同じ学習率で，そのあとは100epoch ごとに線形に減衰していった．

- beta1（著者コードでのdefault値は0.5） 以外は全てdefault 値

## 重み初期化

重みは全て$\mathcal{N}(\mu=0, \sigma=0.02)$で初期化．

Instance Norm(Batch Norm でN=1)では，gammaは$\mathcal{N}(\mu=1.0, \sigma=0.02)$，betaは0で初期化．



## Genetrator 

本研究のGeneratorは２種類（global generator, local enhancer network）でなっている．以下に２つのネットワークの構成を示す．

- Global Network：

  > c7s1-64, d128, d256, d512, d1024, R1024, R1024,
  > R1024, R1024, R1024, R1024, R1024, R1024, R1024,
  > u512, u256, u128, u64, c7s1-3

- Local enhancer：

  > c7s1-32, d642, R64, R64, R64, u32, c7s1-3

ただし，`c7s1-k` はstride=1で filters=k の 7x7 Convolution-InstacneNorm-ReLU layer を表し，`dk`はstride=2, filter=k の　3x3 Convolution-InstanceNorm-ReLU layerを表す．そして，`Rk` は２つの同じfileter=k の3x3 Convolutionを持つResidual Blockであり，`uk` はfilter=k, stride=1/2の 3x3の fractional-striped-Convolution-InstanceNorm-ReLu layerを表す（コードでは次の通り， `nn.ConvTranspose2d(・・・, kernel_size=3, stride=2, padding=1, output_padding=1)`）．またboundary artifactsのため，paddingは全てrefrection paddingを使用．

# 議論はある?

## 研究コード公開リポジトリから確認すべきこと

- ~~Adamの学習率を線形に減衰させていったと書いてあるが，100epochごとにどのくらい減衰させたのか．~~
  - [ここ参照](https://github.com/NVIDIA/pix2pixHD/blob/master/models/pix2pixHD_model.py#L290-L299) ．`self.opt.niter_decay`で選択可能にしているが，default では100epochごとに，以下の式で変更している
$$
\begin{align}
\rm{lrd}^{(t)} &= \rm{lr}^{(t)} / 100\ (\text{=self.opt.niter_decay}) \\
\rm{lr}^{(t+1)} &\leftarrow \rm{lr}^{(t)} - \rm{lrd}^{(t)}
\end{align}
$$
- 入力のSemantic label はone-hot vectorに変換しているようなことを論文に書いてあったが，どうなのかをチェック

  > The instance boundary map is then concatenated with the **one-hot vector** representation of the semantic label map, and fed into the generator network. 

  - 著者のコードに以下のように書いてあったので，label map はone-hot vectorになるようにする．

    > - If you want to train with your own dataset, please generate label maps which are one-channel whose pixel values correspond to the object labels (i.e. 0,1,...,N-1, where N is the number of labels). This is because we need to generate one-hot vectors from the label maps. Please also specity `--label_nc N` during both training and testing.

- Instance Feature Embeding 用のEncoder のネットワーク構成&instance-wise average pooling

  - [呼び出し元（output_ncはdefaultで3?）](https://github.com/NVIDIA/pix2pixHD/blob/master/models/pix2pixHD_model.py#L50-L53)
  - [呼び出し先（実際のネットワーク構成）](https://github.com/NVIDIA/pix2pixHD/blob/master/models/networks.py#L257-L292)

- encoder network $E$ はgenerater，discriminatorと共に学習させるとあるが，それはどのタイミングで？
  $G_1$ (global generator network), $G_2$ (local enhancer network)はまず，$G_1$，その後$G_2$で学習させるが，$G_1$の時点でencoder network $E$ もつなげる？**これはencoder network $E$ の出力サイズを見れば解決しそう．**

  - $G_1$のみの場合はsemantic label, edge mapとともに，Encoderの出力(feature)をchannel-wiseに結合したテンソル（1024x512）を入力としていた．
  - $G_2$をトレーニングする場合はLocal Enhancerにはsemantic label, edge map, feature(2048x1024)を入力する．その時，$G_2$への入力はsemantic label, edge map, feature(1024x512)（$G_2$ で入力されるテンソルを1/2したもの．正確にはdownsampling したもの）が入力される．
    - <https://github.com/haru-256/pix2pixHD/blob/master/models/networks.py#L175>

- 実際に使用している（ハイパラはdefaultで）モデルを理解する．

- GeneratorのOptimizerについて

  - Local Enhancerを学習させる際に，はじめ（20epoch程度）はGlobal を固定する．その後，Globalのパラメータを含む最適化手法を再定義し，用いる．つまり，一度最適化手法を捨てる．
    - https://github.com/NVIDIA/pix2pixHD/blob/master/models/pix2pixHD_model.py#L85-L102
    - https://github.com/NVIDIA/pix2pixHD/blob/master/models/pix2pixHD_model.py#L281-L288
    - https://github.com/haru-256/pix2pixHD/blob/master/train.py#L120-L122

- VGG lossについて

  - https://github.com/NVIDIA/pix2pixHD/blob/master/models/networks.py#L119,%20L126

# 次に読むべき論文は?



## 先行研究

1. Q. Chen and V. Koltun. Photographic image synthesis with cascaded refinement networks.
   In IEEE International Conference on Computer Vision (ICCV), 2017.
1. Johnson, J., Alahi, A., and Li, F. (2016). Perceptual losses for real-time style transfer and superresolution.
   CoRR, abs/1603.08155.