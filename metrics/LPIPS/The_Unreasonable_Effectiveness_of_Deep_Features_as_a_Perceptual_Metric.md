# [The Unreasonable Effectiveness of Deep Features as a Perceptual Metric](https://arxiv.org/abs/1801.03924)

- 著者：Richard Zhang，Philip Isola et al.
- code : https://github.com/richzhang/PerceptualSimilarity
- CVPR2018

# どんなもの？

# 先行研究と比べてどこがすごい？

# 技術や手法のキモはどこ？

# どうやって有効だと検証した？

# 議論はある?

# 次に読むべき論文は？

