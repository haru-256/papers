# 非負値行列因子分解：NFM

# 参考文献

- 亀岡弘和　非負値行列因子分解
   http://www.kecl.ntt.co.jp/people/kameoka.hirokazu/publications/Kameoka2012SICE09published.pdf

- 亀岡弘和　チュートリアル：非負値行列因子分解
  http://hil.t.u-tokyo.ac.jp/publications/download.php?bib=Kameoka2011MUS07.pdf
- 澤田宏　非負値行列因子分解 NMF の基礎と データ／信号解析への応用
  http://www.geocities.jp/mabonakai/doc/nfm.pdf