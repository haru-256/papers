# [Unsupervised Person Image Synthesis in Arbitrary Poses](http://www.iri.upc.edu/files/scidoc/2024-Unsupervised-person-image-synthesis-in-arbitrary-poses.pdf)

author : Albert Pumarola et al.

conference : CVPR2018

# どんなもの？

姿勢変換を教師なしで行う新しい手法を提案した．

具体的には人の画像と，所望の姿勢が2D skelton （OpenPose の棒人間みたいなもの）が与えられたら，その姿勢に変換する手法．

# 先行研究と比べてどこがすごい？

先行研究として[Pose Guided Person Image Generation](https://arxiv.org/abs/1705.09368) （以下 Liqian Ma）を挙げている．

[Liqian Ma](https://arxiv.org/abs/1705.09368)の研究では姿勢変換を教師あり学習として研究していたが，本研究では教師なし（姿勢を変えているペア画像がない）状況下でも適用可能は手法を提案し，[Liqian Ma](https://arxiv.org/abs/1705.09368) で変換された画像とよく似たレベルで姿勢変換が可能であることをDeep Fashion で確認した．

教師なし姿勢変換という問題を２つのサブタスクに分解して取り組んだ．１つ目は姿勢条件付き双方向Generatorについて．２つ目はコンテンツとスタイル項を統合する新しいロス関数について．

# 技術や手法のキモはどこ？
## 問題設定について

本研究は教師なしの姿勢変換なので，Generator は $(I_{p_o}, \boldsymbol{p}_f) \rightarrow I_{p_f}$ の変換関数を学習する．ここで$I_{p_o} \in \mathbb{R}^{3\times H \times W}$ は 姿勢 $\boldsymbol{p}_{o}$ をした人間画像で,$I_{p_f} \in \mathbb{R}^{3\times H \times W}$ は してほしい姿勢 $\boldsymbol{p}_{f}$ をした人間画像．

姿勢 $\boldsymbol{p}$ は18個のkeypoint（$i$番目の部位を$\boldsymbol{u_i} = (u_i, v_i)$ と表した．）からなるbelief map $\boldsymbol{B}_{\boldsymbol{p}}= (\boldsymbol{B}_{\boldsymbol{p}, 1}, \dots, \boldsymbol{B}_{\boldsymbol{p}, M})\in\mathbb{R}^{M \times H \times W}$ で表される，ここで $\boldsymbol{B}_{\boldsymbol{p}, i} \in \mathbb{R}^{H\times W}$ は以下のように表される（論文では詳しくは書いていないが，おそらくこう）．
$$
\boldsymbol{B}_{\boldsymbol{p}, i}[u, v] = \exp \left\{ - \frac{(u -u_i)^2 + (v - v_i)^2}{2 \sigma^2} \right\} , \ \ \forall (u, v) \in \mathcal{U}
$$
ここで $\mathcal{U}$ は画像のピクセル位置 $(u, v)$ の全ての集合を表し，$\sigma^2 = 0.03$ （0.03 は小さすぎない？）である．全ピクセルについてガウスカーネルをかけた感じ．以下は192x128の画像についての例．ただし，$\sigma^2 = 9$とした．

<img src="figures/belief_map.png" width=400></img>

keypoint はおそらくConvolutional Pose Matchine から取られたもの．pose_transfer_fashion550kのkeypoint + 目・耳のkeypoint．

本研究では$i$番目の学習サンプルのセットは $\{ I_{p_o}^i, \boldsymbol{p}_{o}^i, \boldsymbol{p}_{f}^i \}$ と表す．

## 概要

<img src="figures/fig1.png"></img>

以下の４つで構成される．
1. generator $G(I|\boldsymbol{p})$：ある姿勢の人物画像 $I$ を同一人物だが異なる姿勢 $\boldsymbol{p}$ の人物画像を作成する微分可能render ．さらに$G$ は一回の学習に二度使われる．一度目は $I_{\boldsymbol{p}_o} \rightarrow I_{\boldsymbol{p}_f}$ で使われ，二度目は姿勢変換した画像から元の姿勢の画像への変換 $I_{\boldsymbol{p}_f} \rightarrow \hat{I}_{\boldsymbol{p}_o}$ で使われる．
2. pose detector $\phi(I)$：与えられた人物画像の姿勢$\boldsymbol{p}$ を推定するモデル．
3. Discriminator $D_I(I)$： 与えられた画像が生成画像か本物画像かを区別するモデル
4. Loss：person identuty を保つように設計されたloss．本研究では$\hat{I}_{\boldsymbol{p}_o} $と$I_{\boldsymbol{p}_o}$ 間のsemantic content similarity  を維持するloss と，$I_{\boldsymbol{p}_f}$と$I_{\boldsymbol{p}_o}$ 間のstyle similarity （服の色やテクスチャなど）を保つlossの２つを導入した．

## Network

学習するネットワークは３つです．

### Generator $G$

人物画像$I_{\boldsymbol{p}_o}$ と姿勢 $\boldsymbol{p}_f$ を受け取り，姿勢$\boldsymbol{p}_f$ をとった同一人物画像$I_{\boldsymbol{p}_f}$ を生成するモデル

- 入力：人物画像$I_{\boldsymbol{p}_o}$ と姿勢 $\boldsymbol{p}_f$ をチャンネル軸で結合したテンソル$(I_{\boldsymbol{p}_o}, \boldsymbol{p}_f) \in \mathbb{R}^{(N+3)\times H \times W}$  

- 出力：姿勢 $\boldsymbol{p}_f$ をとった同一人物画像$I_{\boldsymbol{p}_f}$ 
- 構造：Cycle GAN で扱われたGenerator．Figure2 を見るかぎり，ResBlock は8個（pix2pixHD で扱ったネットワークと似ている．）

### Discriminator $D$

与えられた画像$I$が生成画像か本物画像かを区別するモデル．生成画像が本物であるように保障するために使う．

- 入力：ある姿勢をとった人物画像 $I$ ．姿勢に関する情報は与えない．
- 出力：$\boldsymbol{Y}_I \in \mathbb{R}^{1 \times 26 \times 26}$ ．各画素値 $\boldsymbol{Y}_I [i, j]$ は与えられた画像のパッチ$ij$ が本物である確率を表す．
- 構造：PatchGAN．Patch のサイズは不明．256x256→26x26となるようなPatch Size

### Pose Detector $\phi$

画像から姿勢を推定するモデル．生成画像の姿勢が指定した姿勢であるように保証するために使う．

- 入力：姿勢$\boldsymbol{p} \in \mathbb{R}^{N \times H \times W}$ をとった人物画像 $I_{\boldsymbol{p}}$
- 出力：姿勢$\boldsymbol{p} \in \mathbb{R}^{N \times H \times W}$
- 構造：おそらく，Cycle GAN で扱われたGenerator

## Loss

ロスは以下の３種類ある．

1. Adversarial Loss：生成画像の分布を本物画像の分布に近づける．条件付きではない．
2. Conditional Pose Loss：生成画像の姿勢が望ましい姿勢になっているように．
3. Identity Loss：生成画像と入力画像で同一人物になるように．これはさらにContent Loss とStyle Lossに分けられる．

### Adversarial Loss $\mathcal{L}_{\mathrm{GAN}}$

LSGAN のloss．式は以下の通り．$\boldsymbol{p}$ は学習データ内で同じものがない．よって，条件付きにしてしまうと，$D$ は$\boldsymbol{p}$ とその姿勢の本物画像$I_{\boldsymbol{p}}$ のペアを暗記してしまえば簡単に本物と偽物の見分けがつく．そうなると $G$は $I_{\boldsymbol{p}}$ を作ろうとしていまい，望ましい$G$ が得られない（$G$ は与えられた$I$ が姿勢変換した画像を作成してほしい）．

実装するときは以下の通りにすることになる．ここで$N$ はバッチ数で，$I_n$ はバッチ内のn番目の画像を，$\boldsymbol{p}_{f,n}$ はバッチ内のn番目の目標姿勢を表す．

- Generator のロス
  $$
  \mathcal{L}_{\mathrm{G-GAN}} = \frac{1}{N} \sum_{n=1}^N \left[ D \left(G(I_n, \boldsymbol{p}_{f,n}) \right) - 1\right]^2
  $$

- Discriminator のロス
  $$
  \mathcal{L}_{\mathrm{D-GAN}} = - \frac{1}{N} \sum_{n=1}^N \left[ \left(D(I_{n}) -1\right)^2 + \left(D \left(G(I_n, \boldsymbol{p}_{f,n}) \right)-0 \right)^2 \right]
  $$

### Conditional Pose Loss $\mathcal{L}_{\mathrm{P}}$

式は以下の通り．
$$
\mathcal{L}_{\mathrm{P}}(G) = \mathbb{E}_{I \sim p(I),\  \boldsymbol{p}_f \sim p(\boldsymbol{p}_f)} \left[ \left\| \phi(G(I, \boldsymbol{p}_f)) - \boldsymbol{p}_f \right\|_2 ^2 \right]
$$
実装するときは以下の通りにすることになる．
$$
\mathcal{L}_{\mathrm{P}}(G) =  \frac{1}{N} \sum_{n=1}^N \left\| \phi(G(I_n, \boldsymbol{p}_{f, n})) - \boldsymbol{p}_{f, n} \right\|_2 ^2
$$

### Identity Loss $\mathcal{L}_{\mathrm{Id}}$

Identity Loss は生成画像の服装，体などが同一になるように保証するLoss．以下の２つからなる．このアイデアは[Gatys](https://zpascal.net/cvpr2016/Gatys_Image_Style_Transfer_CVPR_2016_paper.pdf) らのStyle Transfer の研究に基づいている．

1. Content Loss ： $I_{\boldsymbol{p}_f} = G(I_{\boldsymbol{p}_o}, \boldsymbol{p}_f)$ を元の姿勢に戻した際の画像 $\hat{I}_{\boldsymbol{p}_o} = G(I_{\boldsymbol{p}_f}, \boldsymbol{p}_o)$ が $I_{\boldsymbol{p}_o}$ になるようにするロス．
2. Style Loss ：$G(I_{\boldsymbol{p}_o}, \boldsymbol{p}_f)$  と $I_{\boldsymbol{p}_o}$ のテクスチャ（服装，ヘアスタイルなど）が同一になるようにするロス．

#### Content Loss $\mathcal{L}_{\mathrm{Content}}$

式は以下の通り．ここで，$I_{\boldsymbol{p}_f} = G(I_{\boldsymbol{p}_o}, \boldsymbol{p}_f), \hat{I}_{\boldsymbol{p}_o} = G(I_{\boldsymbol{p}_f}, \boldsymbol{p}_o)$ であり，$\psi_{z}(\cdot)$ はVGG16の$z$番目の層の特徴量を表す．
$$
\mathcal{L}_{\mathrm{Content}}(G) = \mathbb{E}_{I_{\boldsymbol{p}_o} \sim p(I),\ (\boldsymbol{p}_o, \boldsymbol{p}_f) \sim p(\boldsymbol{p}_o, \boldsymbol{p}_f)} \left[ \left\| \psi_{z} (I_{\boldsymbol{p}_o}) - \psi_{z} (\hat{I}_{\boldsymbol{p}_o}) \right\|_2^2 \right]
$$
実装するときは以下の通りにすることになる．
$$
\mathcal{L}_{\mathrm{Content}}(G) = \frac{1}{N} \sum_{n=1}^N \left\| \psi_{z} (I_{\boldsymbol{p}_o, n}) - \psi_{z} (\hat{I}_{\boldsymbol{p}_o, n}) \right\|_2^2
$$
ここで，$I_{\boldsymbol{p}_f, n} = G(I_{\boldsymbol{p}_o, n}, \boldsymbol{p}_{f, n}), \hat{I}_{\boldsymbol{p}_o, n} = G(I_{\boldsymbol{p}_f, n}, \boldsymbol{p}_{o})$ つまり，$I_{\boldsymbol{p}_f, n}$ はバッチ内のn番目の本物画像 $I_{\boldsymbol{p}_o}$ と目標姿勢 $\boldsymbol{p}_{f, n}$ を受け取って，Generator $G$ が生成した画像であり，$\hat{I}_{\boldsymbol{p}_o, n}$  は $I_{\boldsymbol{p}_f, n}$ と元の姿勢 $\boldsymbol{p}_{o}$ から生成された画像である． 

単にpixel レベルのロス（例えばL1など）ではなく，Perceptual Loss を取った理由として，著者はL1Loss だとPatch GAN を使用しても高周波成分が上手く生成できなかったからだと主張している（後ほどその結果が示す．）．

#### Style Loss $\mathcal{L}_{\mathrm{Patch-Style}}$

このロスは，$I_{\boldsymbol{p}_{o}}$ と $I_{\boldsymbol{p}_{f}}$ の keypoints の各joint周りが同じテクスチャになるようにするロスである．

style transfer と同様に，VGGの特徴量のチャンネルに関するグラム行列の期待値の差をロスとします．

その前にまず，各jointの周りのVGGの特徴量（$X_{\boldsymbol{p}_o, i}$）を以下のようにして得ます．ここで$\boldsymbol{B}_{\boldsymbol{p_o}, i} \in \mathbb{R}^{1 \times H \times W}$ は$i$番目のjointのberief map を表し，$f(\boldsymbol{B}_{\boldsymbol{p_o, i}}) \in \mathbb{R}^{1 \times H' \times W'} $ はaverage pooling でdown sample した $i$番目のjointのberief map です．  $\psi_{z} (I_{\boldsymbol{p}_o}) \in \mathbb{R}^{C' \times H' \times W'}$ なので，$f(\boldsymbol{B}_{\boldsymbol{p_o, i}}) \odot \psi_{z} (I_{\boldsymbol{p}_o})$ は $f(\boldsymbol{B}_{\boldsymbol{p_o, i}})$ をチャンネル方向にブロードキャストしてから，要素ごとに $\psi_{z} (I_{\boldsymbol{p}_o})$ と積をとることを意味します．
$$
X_{\boldsymbol{p}_o, i} = f(\boldsymbol{B}_{\boldsymbol{p_o, i}}) \odot \psi_{z} (I_{\boldsymbol{p}_o})
$$
そして Style Loss を以下のように計算します．
$$
\mathcal{L}_{\mathrm{Patch-Style}}(G) = \frac{1}{N} \sum_{n=1}^N \frac{1}{M} \sum_{i=1}^M  \left \| \frac{1}{H'W'} (\mathcal{G}_{\boldsymbol{p}_o, i} - \mathcal{G}_{\boldsymbol{p}_f, i})\right\|_2^2
$$

ここで，$\mathcal{G}_{\boldsymbol{p}, i} \in \mathbb{R}^{C' \times C'}$ は以下のように定義されます．ここで $\tilde{X}_{\boldsymbol{p}, i} \in \mathbb{R}^{C' \times H'W'}$ は $X_{\boldsymbol{p}, i} \in \mathbb{R}^{C' \times H' \times W'}$ をreshape したものです．
$$
\mathcal{G}_{\boldsymbol{p}, i} = \tilde{X}_{\boldsymbol{p}, i} \tilde{X}_{\boldsymbol{p}, i}^T
$$
最終的なIdentity Loss は以下の通りになります．
$$
\mathcal{L}_{\mathrm{Id}}(G) = \mathcal{L}_{\mathrm{Content}} (G) + \lambda \mathcal{L}_{\mathrm{Patch-Style}} (G)
$$
ここで $\lambda$ はContent Loss と Style Loss との間の重要度をコントロールするハイパーパラメータです．論文では実験でいくつにしたのか書かれていなかった．



結局，最終的なfull loss は以下の通りになります．

- Discriminator
  $$
  \mathcal{L}_{\mathrm{D-GAN}}(D, \hat{I}_{\boldsymbol{p}_{o}}) + \mathcal{L}_{\mathrm{D-GAN}}(D, I_{\boldsymbol{p}_{f}})
  $$

- Generator
  $$
  \mathcal{L}_{\mathrm{G-GAN}}(G, \hat{I}_{\boldsymbol{p}_o}) + \mathcal{L}_{\mathrm{G-GAN}}(G, I_{\boldsymbol{p}_f}) + \lambda_p \left(\mathcal{L}_{\mathrm{P}}(G, \hat{I}_{\boldsymbol{p}_o}) + \mathcal{L}_{\mathrm{P}}(G, I_{\boldsymbol{p}_f}) \right) + \lambda_{\mathrm{Id}} \mathcal{L}_{\mathrm{Id}}
  $$

- Pose detector
  $$
  \mathcal{L}_{\phi}(\phi) = \frac{1}{N} \sum_{n=1}^N \left \| \phi(I_{\boldsymbol{p}_o,n}) - \boldsymbol{B} _{\boldsymbol{p}_{o},n}\right \|_2^2
  $$

この３つのネットワークを交互に学習していきます．

# どうやって有効だと検証した？

## 実験

### 条件

- LSGAN を使用
- Discriminator の更新に使用する生成画像に過去の生成画像も使用．[手法の研究 Learning from Simulated and Unsupervised Images through Adversarial Training](https://arxiv.org/abs/1612.07828)
- 特徴抽出の $\psi_z$ はVGG16の$z=7$ を使用．明確にはどの層か書いていなかったが，ReLU を一層に数えなければ `relu3_1` で，一層に数えるならば`relu2_1`
- 学習率：Generator=0.0002，discriminator 0.0001
- batch size 12
- Epoch : 300
- 重み：$\lambda_P=700$，$\lambda_{\mathrm{Id}} = 0.3$ （ただし，Content Loss と Style Loss との間の重みは記述なし）
- 学習中，$\boldsymbol{p}_f$ は学習データセットからランダムに選択される．
- 姿勢はConvolutional Pose Machine (CPM) を使用．使用データは不明．OpenPose のCOCO と同じ形式？
- データセット：DeepFashion（In-shop Clothes Retrieval Benchmark）を使用．
  - 前処理：256x256 にリサイズし，three possible flips （よくわからない）を各画像に適用．その後CPMで姿勢を検出し，検出できなかったデータは除外．最後にランダムに24,145枚を学習データに，5,000枚をテストに使用．テストデータセットはある画像に加え，１つのデータに目標姿勢とその本物画像で構成されている．

### 定量的評価

SSIM・IS で以下の４つの教師あり手法と比較した．

1. [Pose Guided Person Image Generation (NIPS 2017)](http://papers.nips.cc/paper/6644-pose-guided-person-image-generation) ：姿勢変換にGANを使用した初めての論文？
2. [Multi-View Image Generation from a Single-View](https://arxiv.org/abs/1704.04886)：以下の姿勢変換
3. [Learning Structured Output Representation using Deep Conditional Generative Models (NIPS 2015)](https://papers.nips.cc/paper/5775-learning-structured-output-representation-using-deep-conditional-generative-models) ：Structured Output Representation？姿勢変換を主な目的にした手法ではない．
4. [Conditional Generative Adversarial Nets](https://arxiv.org/abs/1411.1784) ：条件付きGAN

結果は以下の表の通り．ただし，テストデータは統一されていないので直接的には比較できないが，定量的評価では各手法と比肩する．

<img src="figures/table.png" width=500></img>

### 定性的評価

#### Success cases

以下の利点がみられる．

1. 入力画像とかなり異なる姿勢でも生成する能力がある．以下の図の右から２番目では目標姿勢が180度回転しているが上手くできている．
2. 入力画像ではみられない部分も生成できる．例えば最も右側の場合，入力画像ではみられない足を上手く生成できている．
3. 以下のどの姿勢変換の場合でもブラウスの詳細な部分まで生成できている．これは$\mathcal{L}_{\mathrm{Patch-Style}}$ の影響が出ているとコメントしている．定量的評価で比較した４つの手法ではテクスチャの詳細な部分が消える．

<img src="figures/fig2.png" width=900></img>

#### Failure cases

失敗には大きく分けて以下の４つのタイプがある．

1. 左上：テクスチャが正しく変換できていない．この場合ではオリジナル画像で見られたズボンがlower leg にのみうつっている．
2. 右上：入力画像の顔が姿勢変換後にも現れている．
3. 左下：入力画像の姿勢が正しく目標画像に変換されなかった．この種を作者は'geometric error' と呼んでいる．
4. 入力画像の手が生成画像のテクスチャとしてマッピングされている

<img src="figures/fig3.png" width=900></img>

## Ablation Study
著者はロスのどの要素も欠かせないと言及しています．具体的に結果を見せて言及しているのは$\mathcal{L}_{\mathrm{Id}}$ です．$\mathcal{L}_{\mathrm{Id}}$ を単なる$\hat{I}_{\boldsymbol{p}_o} $と$I_{\boldsymbol{p}_o}$ 間のL1Loss に置き換えると以下の図のような結果になりました．$\hat{I}_{\boldsymbol{p}_o} $ は元画像のテクスチャの低周波成分を保持しますが，$I_{\boldsymbol{p}_f} $は$I_{\boldsymbol{p}_o}$ のidentity を失い，全結果が白いTシャツに青いジーンズを着た平均的な女性（ボヤけている）になっています．

<img src="figures/fig4.png" width=400></img>
## Images with background
背景が真っ白のDeep Fashion で学習したモデルで，背景が複雑な画像の姿勢変換を行ってみました．結果が以下の通りです．

<img src="figures/fig5.png" width=500></img>

# 議論はある？

# 次に読むべき論文は？

