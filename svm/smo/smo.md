# SVM の最適化手法（SMO）について

主にカーネルSVMの最適化手法であるSMO についてまとめる．基本的には [竹内一郎／烏山昌幸・著 「サポートベクトルマシン」](https://www.kspub.co.jp/book/detail/1529069.html) に沿って勉強した結果をまとめる．

ここではSVM において解 $\boldsymbol{\alpha}$  が最適解である必要十分条件についてまとめます．

# はじめに
SVM の最適化問題は目的関数が凸な２次関数，制約式が１次関数からなる凸２次最適化問題（２次計画問題）です．凸２次最適化問題は比較的扱いやすいです．目的関数が凸な２次関数のため，どんな初期値から初めたとしても大域的最適解を得ることができます（というか，極小値が最小値なため局所的最適解が存在しない）．


# 前提知識

[竹内一郎／烏山昌幸・著 「サポートベクトルマシン」](https://www.kspub.co.jp/book/detail/1529069.html) の1.5章まで（~p22）を前提知識とします．



非線形SVMの主問題と双対問題

- 主問題
$$
\begin{align}
\begin{split}
\min_{\boldsymbol{w}, b, \boldsymbol{\epsilon}} &\ \ \frac{1}{2} \|\boldsymbol{w}\|^2 + C\sum_{i \in [n]} \xi \\
s.t.&\  -\Big(y_i (\boldsymbol{w}^{\top} \boldsymbol{\phi}(\boldsymbol{x}_i )+ b -1 + \xi_i\Big) \leq 0, \quad i\in[n] \\
& - \xi_i \leq 0, \quad i \in [n]
\end{split}
\end{align}
$$
- 双対問題
$$
\begin{align}
\begin{split}
\max_{\boldsymbol{\alpha}} &\ \ - \frac{1}{2}\sum_{i,j \in [n]} \alpha_i \alpha_j y_i y_j  K(\boldsymbol{x}_i, \boldsymbol{x}_j) + \sum_{i \in [n]} \alpha_i \\
s.t.&\  \sum_{i \in [n]}\alpha_i y_i = 0\\
& 0 \leq \alpha_i \leq C,\quad i \in [n]
\end{split}
\end{align}
$$

- KKT条件
$$
\begin{align}
\frac{\partial L}{\partial \boldsymbol{w}} = \boldsymbol{w} - \sum_{i \in [n]} \alpha_i y_i \boldsymbol{\phi}(\boldsymbol{x}_i)&= \boldsymbol{0} \\

\frac{\partial L}{\partial b} = - \sum_{i \in [n]} \alpha_i y_i &= 0 \\
 
\frac{\partial L}{\partial b} = C-\alpha_i -\mu_i &= 0 \\

 -\Big(y_i (\boldsymbol{w}^{\top} \boldsymbol{\phi}(\boldsymbol{x}_i )+ b -1 + \xi_i\Big) &\leq 0, \quad i\in[n]\\
 
 -\xi_i &\leq 0, \quad i \in[n]\\
 \alpha_i &\geq 0,\quad i\in[n]\\
 \mu_i &\geq 0, \quad i\in[n]\\
 \alpha_i \Big(y_i (\boldsymbol{w}^{\top} \boldsymbol{\phi}(\boldsymbol{x}_i )+ b -1 + \xi_i\Big) &= 0, \quad i\in[n]\\
 \mu_i \xi_i &= 0, \quad i\in[n]\\
 
\end{align}
$$

- マージンと学習データの位置と双対変数 $\boldsymbol{\alpha}$ の関係


# SVM の解の最適性条件について

３つの添え字集合

$$
\begin{align*}
\mathcal{O} &= \{ i \in [n]\ |\  \alpha_i=0 \} \\
\mathcal{M} &= \{ i \in [n]\ |\  0 \leq\alpha_i \leq C\} \\
\mathcal{L} &= \{ i \in [n]\ |\  \alpha_i=C \} \\
\end{align*}
$$

を定義し，$f(\boldsymbol{x}) = \sum_{i\in[n]} \alpha_i y_i K(\boldsymbol{x}_i, \boldsymbol{x}) + b$ とすると，以下の条件がSVMのKKT条件の必要十分条件となります．
$$
\begin{align}
y_i f(\boldsymbol{x}_i) &\geq 1, \quad i\in \mathcal{O} \\
y_i f(\boldsymbol{x}_i) &= 1, \quad i\in \mathcal{M} \\
y_i f(\boldsymbol{x}_i) &\leq 1, \quad i\in \mathcal{L} \\
\boldsymbol{y}^{\top} \boldsymbol{\alpha} &= 0 \\
\boldsymbol{0} \leq \boldsymbol{\alpha}&\leq C\boldsymbol{1}
\end{align}
$$

KKT条件と比べると $\mu_i$ と $\xi_i$ が消され，より扱いやすくなっています．これが解の最適性として使われます．


# SMOアルゴリズム

SVM は凸最適化問題として定式化されるため，小規模なデータセットに対しては，汎用的な凸最適化手法（内点法など）を使うことができます．しかし，大規模なデータセットに対しては，SVMに特化した最適化手法を使うことで大幅に高速化させることができます．以下ではSVMの双対問題を解くための最適化手法を説明します[^1]．

## 分割法

SVM の双対問題 (2) では $n$ (学習データ数)個の未知変数 $\{\alpha_i\}_{i \in [n]}$ を最適化します．しかし，学習データが膨大な場合，非常に多くの未知変数を持った大規模な最適化問題を解くことになります．そこでサポートベクトルのように分類器への影響が大きいと予想される，一部の学習データからなる部分学習データ集合を考え，この部分学習データ集合（作業集合：working set と呼びます）に関する小規模な最適化問題を解きます．当然ながら，選択した部分学習データ集合が正しいとは限らないので，途中で得られる暫定解を用いて部分学習データ集合を更新し再度，小規模な最適化問題を解くことになります．このような方法は分割法（decomposition method）またはチャンキンング法（chunking method）と呼ばれ，SVMの最適化方法としてよく用いられています．




$$
\begin{align}
\begin{split}
\max_{\boldsymbol{\alpha}} &\ \ - \frac{1}{2}\left( \alpha_s^{2} y_s^2 K_{ss} + 2\alpha_s \alpha_t y_s y_t  K_{st} + \alpha_t^{2} y_t^2 K_{tt} \right) + (\alpha_s y_s + \alpha_t y_t) \\
s.t.&\   \alpha_s y_s + \alpha_t y_t = 0\\
& 0 \leq \alpha_s \leq C \\
& 0 \leq \alpha_t \leq C 
\end{split}
\end{align}
$$



**<span style="color:red">注意 1</span>**

分割法では，ある一定条件のもと，分割法の解の系列がもとの双対問題の最適解に収束すると証明できることが知られています（Googleの検索エンジンで convergence of decomposition methods と検索すると，沢山それらしき論文が見つかります．全然調査していません．）．

## SMO

分割法の一種である SMO アルゴリズム（sequential minimal optimization algorithm）を紹介します．SMOアルゴリズムは作業集合$\mathcal{S}$ のサイズを $|\mathcal{S}| = 2$ とした分割法です．作業集合のサイズを2 とすることで以下の利点が得られ，大規模な問題についても非常に効率的にSVMの学習を行うことができます．

-  各ステップで $\mathcal{S}$ に対する双対問題が解析的に解くことができる．つまり，式(2) を解く際に内点法やアクティブセット法などのソルバーを使用しなくても良くなり，実装も簡潔になる．

**<span style="color:red">注意 1</span>**

よく使われる機械学習パッケージにscikit-learnがあり，scikit-learn のSVM （[SVC](https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html)など）は内部でLIBSVM ソフトウェアを使用しています．LIBSVMはSMOアルゴリズムに基づいていますが，主に以下の点が変更されています．

1. 変数の初期化方法
2. シュリンキングの導入

**<span style="color:red">注意 2</span>**

線形SVM，すなわちカーネル関数として $K(\boldsymbol{x}_i, \boldsymbol{x}_j) = \boldsymbol{x}_i^{\top} \boldsymbol{x}_j
$ を用いた場合には，主変数と双対変数の関係をうまく利用することより，効率的に解くことができるDCDM アルゴリズム（dual coordinate decent algorithm）があります．LIBSVM の線形SVM バージョン である，LIBLINEARではこちらを利用しています．sklearnでもSVC とは別に[LinearSVC](https://scikit-learn.org/stable/modules/generated/sklearn.svm.LinearSVC.html) があります．こちらではLIBLINEAR を使用しているため，線形SVMで（かつ大規模データで）あれば，こちらを使用する方がより高速に解くことができます．

### SMO の概要
1. 初期化：変数 $\{\alpha_i\}_{i \in [n]}$ を0 に初期化し，作業集合（ある２変数 $\alpha_s, \alpha_t$ ）をランダムに選択．
2. 停止条件（KKT 条件）を満たすまで以下を行う．
   1. その作業集合（２変数）についての最適化問題題を解く（解析的に解ける）．
   2. 得られた暫定解を用いて作業集合を更新（２変数を選択）
3. $0 < \alpha_i < C $ であるようなデータを用いて，バイアス$b$ を以下の式で計算する[^2]．
   $$
   b = y_j - \sum_{i \in [n]} \alpha_{i} y_i \boldsymbol{x}_i^{\top}\boldsymbol{x}_j, \quad j \in \{i \in [n]\  |\  0 < \alpha_i < C\}
   $$

### ２変数の最適化

まず式の簡単のために，双対変数 $\{\alpha_i\}_{i \in [n]}$ を変数変換して，
$$
\beta_i = y_i \alpha_i, \quad i \in [n]
$$
を導入します．ラベルは $y_i \in \{\pm 1\}$ なので，$\alpha_i$ と $\beta_i$ の間には以下の関係があります．
$$
y_i \beta_i = y_i^2 \alpha_i = \alpha_i, \quad i \in [n]
$$
変数 $\{\beta_i\}_{i \in [n]}$ を用いると，双対問題 (2) は，
$$
\begin{align}
\min_{\boldsymbol{\beta}} \quad& \frac{1}{2} \sum_{i, j \in [n]} \beta_i \beta_j K_{ij} - \sum_{i \in [n]} y_i \beta_i \\
s.t.\quad & \sum_{i \in [n]} \beta_i = 0 \\
& 0\leq \beta_i \leq C, \quad \forall i \in \{ [n] \ |\ y_i = +1\} \\
 -&C\leq \beta_i \leq 0, \quad \forall i \in \{ [n] \ |\ y_i = -1\}
\end{align}
$$
と表されます．ここで，$K_{ij} = K(\boldsymbol{x}_i, \boldsymbol{x}_j)$ を表します． 

SMO アルゴリズムでは，$n$ 個の未知変数 $\{\alpha_i\}_{i \in [n]}$ のうち，相異なる２つの変数 $\beta_s. \beta_t, s \neq t$ を作業集合とし，その他の変数は定数扱いとなります．２変数を，
$$
\beta_s \leftarrow \beta_s + \Delta \beta_s,\quad \beta_t \leftarrow \beta_t + \Delta \beta_t
$$
と更新する場合を考えます．このとき，等式制約条件(21) を満たすためには，
$$
\beta_s + \beta_t = (\beta_s + \Delta \beta_s) + (\beta_t + \Delta\beta_t)
$$
が必要であり，
$$
\Delta \beta_t = - \Delta \beta_s
$$
を満たす必要があります．つまり，２変数 $ \beta_, \beta_t $ のみを作業集合とした双対問題を解く場合，実質的に自由に動ける変数は $\Delta \beta_s$ の１変数となります．

続いて，$\Delta \beta_s$ の満たすべき範囲を考えます．更新後の$\beta_s, \beta_t$ が不等式制約 (22), (23) を満たすためには，
$$
\begin{align}
\begin{split}
0 \leq \beta_s + \Delta\beta_s \leq C \quad y_s=+1\ の場合 \\
-C \leq \beta_s + \Delta\beta_s \leq 0 \quad y_s=-1\ の場合
\end{split}
\end{align}
$$
となるように， $\Delta\beta_s$ に制約を加えなければなりません．式(22), (23) と $y_s$ と $y_t$ の符号のそれぞれの組み合わせについて考えると，$\Delta \beta_s$ は，
$$
\begin{align}
\begin{split}
\max(-\beta_s, \beta_t -C) \leq \ &\Delta\beta_s \leq \min(C-\beta_s, \beta_t) \quad y_s=+1, y_t=+1\ \text{の場合} \\
\max(-\beta_s, \beta_t) \leq \ &\Delta\beta_s \leq \min(C-\beta_s, C+\beta_t) \quad y_s=+1, y_t=-1\ \text{の場合} \\
\max(-C-\beta_s, \beta_t -C) \leq \ &\Delta\beta_s \leq \min(-\beta_s, \beta_t) \quad y_s=+1, y_t=+1\ \text{の場合} \\
\max(-C-\beta_s, \beta_t) \leq \ &\Delta\beta_s \leq \min(-\beta_s, C+\beta_t) \quad y_s=+1, y_t=+1\ \text{の場合} \\
\end{split}
\end{align}
$$
を満たす必要があります．

よって，作業集合に対する双対問題 (20) ~ (23) に $\beta_s \leftarrow \beta_s + \Delta \beta_s$ と式(24)を用い， $ \Delta \beta_s$ に関する制約付き最適化問題として（つまり，$\Delta \beta_s$ 以外の項は定数扱い）書き直すと，
$$
\begin{align}
\begin{split}
\min_{\Delta \beta_s} &\quad \frac{1}{2} (K_{ss} - 2 K_{st} + K_{tt})\Delta \beta_s^2 - (y_s - \sum_{i \in [n]} \beta_iK_{is} - y_t + \sum_{i \in [n]} \beta_i K_{it})\Delta \beta_s \\
s.t. &\quad L \leq \Delta \beta_s \leq U
\end{split}
\end{align}
$$
と表すことができます．ここで，$L$ と $U$ は $y_s$ と $y_t$ の符号に応じた式(28) の下限と上限をそれぞれ表します． 式(29) は１変数$\Delta \beta_s$ の制約付き２次関数最小化問題であるので，最適解が
$$
\Delta \beta_s = \begin{cases}
L \quad &L> \frac{y_s - y_t - \sum_{i \in [n]} \beta_i (K_{is} - K_{it})}{K_{ss} - 2K_{st} + K_{tt}} \ \text{の場合}\\
U \quad &U< \frac{y_s - y_t - \sum_{i \in [n]} \beta_i (K_{is} - K_{it})}{K_{ss} - 2K_{st} + K_{tt}} \ \text{の場合}\\
\frac{y_s - y_t - \sum_{i \in [n]} \beta_i (K_{is} - K_{it})}{K_{ss} - 2K_{st} + K_{tt}} \quad
&\ \text{その他の場合}
\end{cases}
$$
と解析的に得ることができます．

### 停止条件

ここでは，作業集合に関するSVM の双対問題を解き，得た解 $\alpha_i$ が，SVM の最適解であることをチェックするための条件（つまり，停止条件）を考えます．すると，式(12) ~ (16) より
$$
\begin{align}
\begin{split}
\alpha_i = 0 \ \Rightarrow  \ y_i f(\boldsymbol{x}_i) \geq 1 \\
0 < \alpha_i < C\ \Rightarrow  \ y_i f(\boldsymbol{x}_i) = 1 \\
\alpha_i = C \ \Rightarrow  \ y_i f(\boldsymbol{x}_i) \leq 1 \\
\end{split}
\end{align}
$$
が条件であることが分かります．ここで，式(15), (16) に関する制約がなくなっていることに気づきますが，これらの制約式は双対問題の制約式そのものです．よって，双対問題を解いて得られた解 $\alpha_i$ は必ず式(15), (16) を満たすため，式(15), (16) に関する制約式がないのです．

最適性条件(31)  を整理し直すため，以下の５つの集合を定義します．
$$
\begin{align*}
\mathcal{L}_0 (\boldsymbol{\alpha}) &= \{ i\ |\ 0 < \alpha_i <C\} \\
\mathcal{L}_1 (\boldsymbol{\alpha}) &= \{ i\ |\ \alpha_i = 0,\ y_i = +1\} \\
\mathcal{L}_2 (\boldsymbol{\alpha}) &= \{ i\ |\ \alpha_i = 0,\ y_i = -1\} \\
\mathcal{L}_3 (\boldsymbol{\alpha}) &= \{ i\ |\ \alpha_i = C,\ y_i = +1\} \\
\mathcal{L}_4 (\boldsymbol{\alpha}) &= \{ i\ |\ \alpha_i = C,\ y_i = -1\} 
\end{align*}
$$
ただし，これらの集合は最適化途中の暫定解$\boldsymbol{\alpha}$ に基づいて定義されているため，$\boldsymbol{\alpha}$ の関数として表記しています．双対表現において
$$
f(\boldsymbol{x}_i) = \sum_{j \in [n]} y_j \alpha_j K_{ij} + b, \quad i \in [n]
$$
と表されることを用いると，式(31) の最適性条件は
$$
\begin{align}
y_i - \sum_{j \in [n]} y_j \alpha_j K_{ij} &= b \quad i \in \mathcal{L}_0(\boldsymbol{\alpha}) \ \text{の場合} \\
y_i - \sum_{j \in [n]} y_j \alpha_j K_{ij} &\leq b \quad i \in \mathcal{L}_1(\boldsymbol{\alpha}) \cup \mathcal{L}_4(\boldsymbol{\alpha}) \ \text{の場合} \\
y_i - \sum_{j \in [n]} y_j \alpha_j K_{ij} &\geq b \quad i \in \mathcal{L}_2(\boldsymbol{\alpha}) \cup \mathcal{L}_3(\boldsymbol{\alpha}) \ \text{の場合} \\
\end{align}
$$
と書き直すことができます．これらをさらにまとめるために，
$$
\begin{align*}
\mathcal{L}_{\text{up}} (\boldsymbol{\alpha}) &= \mathcal{L}_0 (\boldsymbol{\alpha}) \cup \mathcal{L}_1 (\boldsymbol{\alpha})\cup\mathcal{L}_4 (\boldsymbol{\alpha}) \\
\mathcal{L}_{\text{low}} (\boldsymbol{\alpha}) &= \mathcal{L}_0 (\boldsymbol{\alpha}) \cup \mathcal{L}_2 (\boldsymbol{\alpha})\cup\mathcal{L}_3 (\boldsymbol{\alpha})
\end{align*}
$$
と定義すると，最適性条件は
$$
\begin{align}
\begin{split}
y_i - \sum_{j \in [n]} y_j \alpha_j K_{ij} \leq b \quad i \in \mathcal{L}_{\text{up} (\boldsymbol{\alpha})} \ \text{の場合} \\
y_i - \sum_{j \in [n]} y_j \alpha_j K_{ij} \geq b \quad i \in \mathcal{L}_{\text{low} (\boldsymbol{\alpha})} \ \text{の場合}
\end{split}
\end{align}
$$
となります．また，
$$
\begin{align}
m(\boldsymbol{\alpha}) = \max_{i \in \mathcal{L}_{\text{up}}(\boldsymbol{\alpha})} y_i - \sum_{j \in [n]} y_j \alpha_j K_{ij} \\
M(\boldsymbol{\alpha}) = \min_{i \in \mathcal{L}_{\text{low}}(\boldsymbol{\alpha})} y_i - \sum_{j \in [n]} y_j \alpha_j K_{ij}
\end{align}
$$
を導入すると，結局，最適性条件(31) は
$$
m(\boldsymbol{\alpha}) \leq b \leq M(\boldsymbol{\alpha})
$$
を満たすような $b$ が存在することと等価となります．つまり，アルゴリズムの各ステップで$m(\boldsymbol{\alpha})$ と $M(\boldsymbol{\alpha})$ を計算し，$m(\boldsymbol{\alpha}) \leq M(\boldsymbol{\alpha})$ が満たされていれば最適性条件を満たします．

数値上の許容度を $\delta$ （scikit-learn ではdefault で0.001）とすると，解 $\boldsymbol{\alpha}$ が
$$
m(\boldsymbol{\alpha}) - M(\boldsymbol{\alpha}) \leq \delta
$$
を満たしたときに停止するようにします（停止条件）．

### ２変数の選択

2変数 $\beta_s, \beta_t$ の選択方法は様々です．しかし，各ステップでこれらの２変数をランダムに選んだとしてもSMOアルゴリズムは最適解に収束することが知られています（かなり強い前提ですが，調査していません）．ただし，うまく$\beta_s, \beta_t$ を選部ことにより，収束の速さを改善できることが知られています． 2変数の選択はヒューリスティックであり，実験的に速さが改善されることが知られており，証明はできません．

２変数の選択の方法はランダム以外に複数ありますが，本書で紹介された手法を説明します．

２変数を選択する方針としては，式(31) を最も満たしていないような２つの変数のペアを選択することです．式（36）より，最も最適性条件を満たしていないようなペア $s, t$は
$$
\begin{align}
\begin{split}
 s &= \underset{i \in \mathcal{L}_{\text{up}} (\boldsymbol{\alpha})}{\operatorname{argmax}} \Big( y_i - \sum_{j \in [n]} y_j \alpha_j K_{ij}\Big) \\
  t &= \underset{i \in \mathcal{L}_{\text{low}} (\boldsymbol{\alpha})}{\operatorname{argmin}} \Big( y_i - \sum_{j \in [n]} y_j \alpha_j K_{ij}\Big)
\end{split}
\end{align}
$$
と選ばれます．なお，式(41) を求める際に
$$
u_i = y_i - \sum_{j \in [n]} y_j \alpha_j K_{ij}, \quad i \in [n]
$$
を計算する必要があります．これを特に工夫せず行うと$\mathcal{O} (n^2)$ の計算コストがかかってしまいます．しかし，SMOアルゴリズムでは $\alpha_s$ と $\alpha_t$ の２変数のみが更新されているので，$\mathcal{O} (n)$ の計算コストで交信が可能となります．具体的には，更新前と後の $u_i$ をそれぞれ $u_i^{(\text{new})}, u_i^{(\text{old})}$ と表すと，式(42)は
$$
u_i^{(\text{new})} = u_i^{(\text{old})} - (K_{is} -K_{it}) \Delta \beta_s, \quad i \in [n]
$$
と計算できます．ここでは式(18), (26) を適用しました．

### SMO アルゴリズムのまとめ

以下に，SMOアルゴリズムの流れを示します．

**<span style="color:red">SMO アルゴリズム</span>**

入力：学習データ $\{ (\boldsymbol{x}_i, y_i) \}_{i \in [n]}$，正則化パラメータ $C$，カーネル関数 $K$，許容度 $\delta$
出力：双対問題の最適解 $\{ \alpha_i \}_{i \in [n]}$, バイアス $b$

1. **初期化** ：$\beta_i \leftarrow 0, i \in [n]$, $u_i \leftarrow 0, i \in [n]$  ，異なる２つの事例 $s, t \in [n]$ をランダムに選択
2. **while** ($\boldsymbol{\alpha}$ が停止条件(126) を満たしていない：KKT条件を満たさない) **do**
   1. 式(30) により $\Delta \beta_s$ を計算
   2. $\beta_s \leftarrow \beta_s + \Delta \beta_s,\quad \beta_t \leftarrow \beta_t + \Delta \beta_t$
   3. 式(43) より，$u_i, i \in [n]$ を更新
   4. 式(41) より，作業集合 $s, t$ を更新
3. **end while**
4. $\alpha_i \leftarrow y_i \beta_i, i \in [n]$
5. $0 < \alpha_i < C $ であるようなデータを用いて，バイアス$b$ を以下の式で計算する．
   $$
   b = y_j - \sum_{i \in [n]} \alpha_{i} y_i \boldsymbol{x}_i^{\top}\boldsymbol{x}_j, \quad j \in \{i \in [n]\  |\  0 < \alpha_i < 　C\}
   $$










[^1]: カーネルSVMでは主問題を解くことができないため双対問題を解くしかない．しかし，線形SVMの場合，主問題と双対問題のどちらを解いても良く，主問題と双対問題の関係をうまく利用することでカーネルSVMよりも効率的な学習のできるアルゴリズム（DCDM）が開発されている．

[^2]: 実際の実装では数値計算上の安定性を考慮し，条件を満たす全ての $j$ を用いて平均をとることもある．
# 参考文献

- 竹内一郎／烏山昌幸・著 「サポートベクトルマシン」
  https://www.kspub.co.jp/book/detail/1529069.html
- Y, Suhara「SMO徹底入門」
  https://www.slideshare.net/sleepy_yoshi/smo-svm